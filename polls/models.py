from django.db import models
from django.utils import timezone
import datetime


class Question(models.Model):
    ELECTIONS = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')
    def __unicode__(self):
            return self.ELECTIONS
    def was_published_recently(self):
        now = timezone.now()
        return now - datetime.timedelta(days=1) <= self.pub_date <= now

class Choice(models.Model):
    ELECTION = models.ForeignKey(Question)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)
    def __unicode__(self):
        return self.choice_text
