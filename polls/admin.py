from django.contrib import admin

from .models import Choice, Question


class ChoiceInline(admin.TabularInline):
    model = Choice
    extra = 3


class QuestionAdmin(admin.ModelAdmin):
    list_display = ('ELECTIONS', 'pub_date')
    list_filter = ['pub_date']
    search_fields = ['ELECTIONS']

    fieldsets = [
        (None,               {'fields': ['ELECTIONS']}),
        ('Date information', {'fields': ['pub_date'], 'classes': ['collapse']}),
    ]
    inlines = [ChoiceInline]

admin.site.register(Question, QuestionAdmin)
