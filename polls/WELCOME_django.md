
# Create your models here.# Django supports all the common database relationships: many-to-one, many-to-many and one-to-one.

#  we’ve set the default value of votes to 0.

# Create a database schema (CREATE TABLE statements) for this app.
# Create a Python database-access API for accessing Question and Choice objects.

# Each field is represented by an instance of a Field class – e.g., CharField for character fields and DateTimeField for datetimes. This tells Django what type of data each field holds.

# Some Field classes have required arguments. CharField, for example, requires that you give it a max_length. That’s used not only in the database schema, but in validation, as we’ll soon see.

# Migrations for 'polls':
  0001_initial.py:
    - Create model Question
    - Create model Choice
    - Add field question to choice

By running makemigrations, you’re telling Django that you’ve made some changes to your models (in this case, you’ve made new ones) and that you’d like the changes to be stored as a migration

# The sqlmigrate command takes migration names and returns their SQL:

$ python manage.py sqlmigrate polls 0001

# python manage.py migrate
Operations to perform:
  Synchronize unmigrated apps: staticfiles, messages
  Apply all migrations: admin, contenttypes, polls, auth, sessions
Synchronizing apps without migrations:
  Creating tables...
    Running deferred SQL...
  Installing custom SQL...
Running migrations:
  Rendering model states... DONE
  Applying polls.0001_initial... OK

# remember the three-step guide to making model changes:

    +Change your models (in models.py).
    +Run python manage.py makemigrations to create    migrations for those changes
    +Run python manage.py migrate to apply those changes to the database.




# There’s a command that will run the migrations for you and manage your database schema automatically - that’s called migrate, and we’ll come to it in a moment - but first, let’s see what SQL that migration would run. The sqlmigrate command takes migration names and returns their SQL:

$ python manage.py sqlmigrate polls 0001

# manage.py sets the DJANGO_SETTINGS_MODULE

# To invoke the Python shell, use this command:

  $ python manage.py shell
